# What is the New Open edX Theme by OpenCraft?

OpenCraft would like to develop a new open-source Open edX theme, that is modern, slick and easy-to-use. We want to create a unique UX and UI for the entire course experience, from the brochure-site-type pages (eg. home, about, donate etc), right through to the course pages.

The aim is to involve the [Open edX community](https://discuss.openedx.org/) throughout the process to ensure the theme's UX and UI is something both course creators, and their users love. We'd also love the community to contribute to the project if they wish.

# Why are we doing this?

There’s an almost endless supply of online courses to learn new skills and deepen existing knowledge. But how user-friendly are online learning platforms, really? And do they do justice to their high-quality course content? We'd love to give people a beautiful theme that will help elevate their courses online. 

# What are the goals of the theme?

- It should be easy-to-use and conform to UX best practices
- It should be modern and visually appealing
- It should look consistent on all parts of edx-platform, including MFAs
- It should be easy to install manually and come with sensible defaults
- It should be easily tweaked via variables
- It should also be the default theme for OpenCraft's [Ocim](https://ocim.opencraft.com/en/latest/) the ["Pro & teacher plan"](https://opencraft.com/hosting/)

# Want to get involved in the conversation?

Visit the following posts on the Open edX forum, and tell us what you think (we'll add more posts as we go):

- [What do you love or hate about the MOOC platforms you’ve used?](https://discuss.openedx.org/t/what-do-you-love-or-hate-about-the-mooc-platforms-you-ve-used/4905)
- [Wireframing a New Open edX Theme](https://discuss.openedx.org/t/wireframing-a-new-open-edx-theme/5944)


# Prerequisites:

- Enable the branding API by adding an enabled Branding API Config instance via the Django Admin.
- Add the new OpenCraft [frontend-component-footer](https://gitlab.com/opencraft/dev/frontend-component-footer).

# List of available SASS variables for customization

Below is the list of available SASS variables for customization. Check `common/_calm_colors.scss` for the details of what these variables are used for and as the authoritative source for the list of variables.

```scss
$font-family-sans-serif

$active-color
$muted-color

$header-bg
$footer-bg
$body-bg
$section-bg
$body-border-color

$font-color
$font-color-h1
$font-color-nav
$font-color-nav-hover
$font-color-footer
$font-color-footer-hover
$font-color-alt-section

$font-color-headings
$font-color-active

$btn-hover-bg
$btn-hover-border-color
$btn-hover-color

$btn-primary-bg
$btn-primary-color
$btn-primary-border-color
$btn-default-bg
$body-padding
```

To use on the devstack, you need to modify `/common/_variables.scss` to import your scss overrides.

You can then install and use it as a regular theme. Run `make dev.static.lms` to make
sure that the latest changes are picked up.

# Use as branding package for MFEs
This can also be used as a [branding package for MFEs](
  https://open-edx-proposals.readthedocs.io/en/latest/oep-0048-brand-customization.html).
To do so install this package as an override to `@edx/brand`. For now modifications aren't explicitly 
enabled.

If you're using `edx/configuration` for deployment, you can also have this theme applied to MFEs by
adding it as an npm override from the location where it's already installed. You can do so by adding
the following ansible config:

```yaml
MFE_DEPLOY_NPM_OVERRIDES:
  - "@edx/brand@file:/edx/var/edxapp/themes/open-edx-theme/"
```
