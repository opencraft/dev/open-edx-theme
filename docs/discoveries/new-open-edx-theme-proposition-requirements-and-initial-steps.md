# Introduction

Opencraft would like to develop a modern, slick and easy-to-use Open edX theme. The new theme will be the default theme used for all Ocim instances. The work involved will include revamping the design of Opencraft’s current Open edX theme, from the brochure-site-type pages (eg. home, about, donate etc), right through to the course pages.

The goal of this discovery is to scope the UX/UI tasks, and estimate, as accurately as possible, the monthly UX/UI work that will be required to implement the theme iteratively.

# Project Approach

Below is a preliminary list of the main steps we expect to follow for this project. They are listed roughly in the intended order of implementation; however, this order may change. This is because we are taking an iterative approach in which the goals of the project are constantly adjusted based on feedback from community feedback, usability testing, new ideas, unexpected realisations etc.

Since the UX/UI team and the development team will be working closely together, the tasks have been grouped in stages with the intention of giving each team manageable chunks of work to tackle each sprint.

Together with Xavier Antoviaque, Developers will be the reviewers on UX/UI tasks. In turn, Fixate (Ali/Cassie) will be the 2nd reviewers on development tasks. This way, developers can provide feedback on what can be achieved during their next sprint. From there, Fixate will write stories for the developers during sprint planning, and the epic owner will refine and/or split them to take technical constraints into account.

Note: It was mentioned that it would be ideal to implement smaller, generic stylistic changes first. However in order for these types of design changes to be realised, we need to work on the most important pages and features of the theme first. This will help us determine a holistic design approach.

**Proposed steps:**

### 1. Community Feedback

We suggest kicking off the project by seeking community feedback. This will help to validate some of the ideas we have in mind for the Open edX Theme. It will also help us decide on the best way to approach the first feature we implement. We'll seek feedback in the following ways: 

**Use the official forum thread: [https://discuss.openedx.org](https://discuss.openedx.org) to engage with members and discuss:** 
- UX or UI issues they’re facing
- UX or UI wishes or requests
- Current theming system issues they’re facing
- Any particular LMS’s they find intuitive and easy to use (ie. other than Open edX)

**Reaching out to current Ocim users to ask about their needs and frustrations related to theming. We could possibly use the meetings that might result from the [marketing emails](https://tasks.opencraft.com/browse/BB-3678) to gain these insights.** 
    

### 2. Wireframe: Course Items

Once we’ve gathered enough insight from the community, we suggest wireframing the Course item pages first, as this is the most crucial part of the theme. This includes:

- Course Item (not enrolled)
- Course Item (enrolled)
- Course Navigation

We’ll present the wireframes to the team, as well as members on the Opencraft forum, and Open edX forum for community feedback. Any feedback will be incorporated into the designs.
  

### 3. UI Design: Course Items

Once the Course Item UX is approved, we’ll work on the UI of the following:

 - Course Item (not enrolled)
 - Course Item (enrolled)
 - Course Navigation

When applying the UI, we’ll need to take into account which elements on the pages we’ll allow users to customise (eg. colours, typography, buttons). We’ll need to ensure the design accounts for these types of stylistic changes.

We’ll present the UI to the team, as well as members on the Opencraft forum, and Open edX forum for community feedback. This will help us ascertain feedback on the style direction. Any feedback will be incorporated into the designs.

### 4. Style Guide

To ensure the UI direction is easily maintainable, we’ll put together a static style guide that details the various design elements and patterns of the theme. Because the theme will be designed and built iteratively, elements will be added and changed throughout the design process.

The style guide will ensure that all visual styles such as headers, links, buttons, typography, and colours are used consistently across all pages of the theme. We’ll also need to map out the customisation options available to the user within the style guide.

### 5. Wireframe: Site Navigation and Course Lists

At this point we’ll move onto wireframing the rest of the key pages. This includes:

- Site Navigation
- Course List
- Course Dashboard

We’ll present the wireframes to the team, as well as members on the Opencraft forum, and Open edX forum for community feedback. Any feedback will be incorporated into the designs.

### 6. UI Design: Site Navigation and Course Lists

Using the style guide, we’ll apply the UI to these additional pages and elements:

- Site Navigation
- Course List
- Course Dashboard

Any changes or additions made to the design will be updated in the style guide.

We’ll present the UI to the team, as well as members on the Opencraft forum, and Open edX forum for community feedback. This will help us ascertain feedback on the style direction. Any feedback will be incorporated into the designs.

### 7. Wireframe: Home, Sign up, Log in

Here we’ll wireframe the rest of the important pages. These include:

 - Home
 - Sign up
 - Log in
 - Forgot password

We’ll present the wireframes to the team, as well as members on the Opencraft forum, and Open edX forum for community feedback. Any feedback will be incorporated into the designs.
    
### 8. UI Design: Home, Sign up, Log in

Using the style guide, we’ll apply the UI to these additional pages and elements:

 - Home
 - Sign up
 - Log in
 - Forgot password

Any changes or additions made to the design need to be updated in the style guide.

We’ll present the UI to the team, as well as members on the Opencraft forum, and Open edX forum for community feedback. This will help us ascertain feedback on the style direction. Any feedback will be incorporated into the designs.

### 9. Emails

Write and design the emails students will receive:  

 - Sign up
 - Log in
 - Forgot password
 - Course welcome
 - Course completed

*Note: We’ll discuss if the email templates should be custom designed. The templates will need to use open source components.


### 10. UI Design: Account

Once the key pages have been completed, we’ll style the remaining pages:

 - Search Results
 - Account
	 - Profile
	 - Settings
		 - Account Information
		 - Linked Accounts
		 - Order History
 - About
 - Contact
 - Donate
 - Privacy Policy
 - Terms of Service
 - Honor Code

We’ll present the UI to the team, as well as members on the Opencraft forum, and Open edX forum for community feedback. This will help us ascertain feedback on the style direction. Any feedback will be incorporated into the designs.

# Mobile versions

The mobile version of each desktop layout will be designed once the desktop version has been approved. This will allow the developers to implement desktop layouts first, followed closely by mobile layouts.
 
Mobile styles will also be documented in the style guide.

We’ll present the mobile versions of each UI to the team, as well as members on the Opencraft forum, and Open edX forum for community feedback. This will help us ascertain feedback on the style direction. Any feedback will be incorporated into the designs.

# Usability Testing

A number of sprints will be dedicated to usability testing. The goal of usability testing is to get feedback from Open edX users (ie. learners, instructors), Opencraft team members, and other external users, while the application is being developed. This feedback will inform any changes that need to be made to the current UI, as well as the design direction taken in future sprints.

For each usability test, we’ll select a portion of the theme that has been developed (this can be in the form of a prototype, or a fully-fledged feature). We then test the feature with a handful of participants (a sample size of 5 participants is usually sufficient) through moderated or unmoderated usability testing. Each test will be recorded for further evaluation. 

Once all the usability tests have been conducted, Fixate will put together a list of the main points that were mentioned. From there, we’ll adjust the design files accordingly. Depending on the feedback provided, future sprints may need to be adjusted to account for the scope of the proposed solution.

# High-level Prioritised List

In accordance with the Project Approach outlined above, sprints should be planned with the following points in mind:

1.  Development is scheduled based on the UX/UI designs
    
2.  Changes are tested and UX reviewed before any merge
    
3.  Key pages and features will undergo usability testing (with internal, and external users)
    
4.  Each set of usability testing will likely span 2 sprints, as it includes preparing the tests, conducting them, evaluating the feedback, and adjusting design files accordingly

Below is a high-level list that outlines the proposed order of implementation, however, this order will likely be adjusted as the project progresses. UX sprints will remain flexible, and can change according to how development sprints have been structured.

1.  Community Feedback
    
2.  Wireframe: Course Items
    
3.  UI Design: Course Items
    
4.  Usability Testing
    
5.  Style Guide
    
6.  Wireframe: Site Navigation and Course Lists
    
7.  UI Design: Site Navigation and Course Lists
    
8.  Update Style Guide
    
9.  Wireframe: Home, Sign up, Log in
    
10.  UI Design: Home, Sign up, Log in
    
11.  Update Style Guide
    
12.  Emails
    
13.  Usability Testing
    
15.  UI Design: Account
    
16.  Update Style Guide
    
17.  Usability Testing
    

# Monthly UX Time Commitment

We estimate a total of **80 - 100 hours per month** will be required for the UX and UI portion of this project. In terms of a proposed start date, when do you estimate development sprints can begin? Once we have a better understanding of your proposed start date, we can schedule and propose the first UX sprint.

**Please note:**

-   Usability testing will incur additional 3rd party costs if we are required to recruit external users to test the application.
-   This estimated number of UX hours that can be dedicated to this project monthly does not take into account:
-   UX hours that may potentially be required on other OpenCraft projects. If suitable, allocation of UX hours can remain flexible depending on the priority of the projects.   
-   Leave time

# Project Roles

-   Product management: Fixate (Ali/Cassie), reviewed by Xavier
-   Reviewers on UX/UI tasks: Xavier Antoviaque and Developers
-   2nd reviewers on development tasks: Fixate (Ali/Cassie)
